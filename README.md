# Logo

LOGO is a robot focused on automating the shipping process for companies in the logistics market. It extracts the sent shipping order as a pdf file by email and generates the financial information.